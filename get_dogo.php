<?php
        //$ch = curl_init();

function razas(){
$data = json_decode(file_get_contents( "https://dog.ceo/api/breeds/list/all"));
return $data->message;
}


function get_img ($raza){
        $img = json_decode(file_get_contents( "https://dog.ceo/api/breed/$raza/images/random"));     
        return $img->message;
}

function get_subraza_img($raza,$subrazas){
        $AllImg = json_decode(file_get_contents( "https://dog.ceo/api/breed/$raza/images"),true);     
        $AllImg = $AllImg['message'];
        $img_subrazas = [];
        foreach ($AllImg as $key => $value) {
                $posicion_coincidencia = strrpos($value,$subrazas);
                if ($posicion_coincidencia) {
                        array_push($img_subrazas,$value);
                }
        }
        $random_number = rand(0,count($img_subrazas));
        $random_img = (isset($img_subrazas[$random_number])) ?  $img_subrazas[$random_number] : null;
        
        return $random_img;
}

function get_subraza($raza){
        $data = json_decode(file_get_contents( "https://dog.ceo/api/breeds/list/all"),true);
        return $data['message'][$raza]; 
}

function Alfabetic(){
        return [
                'a'=>[],
                'b'=>[],
                'c'=>[],
                'd'=>[],
                'e'=>[],
                'f'=>[],
                'g'=>[],
                'h'=>[],
                'i'=>[],
                'j'=>[],
                'k'=>[],
                'l'=>[],
                'm'=>[],
                'n'=>[],
                'ñ'=>[],
                'o'=>[],
                'p'=>[],
                'q'=>[],
                'r'=>[],
                's'=>[],
                't'=>[],
                'u'=>[],
                'v'=>[],
                'w'=>[],
                'x'=>[], 
                'y'=>[],
                'z'=>[]
        ];
}