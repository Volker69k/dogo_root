<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Dogo Root</title>
  </head>
  <body>
  <div class="container-fluid p-5 bg-dark text-white text-center" background-color="red">
    <h1>Dogo Root</h1>
    <p>Selecciona tu la raza de un Dogo</p> 
  </div>
  <br/>
  <div class="container">
  <div class="row">
    <div class="col">
    <?php  include "razas.php"?>
    </div>
    <div class="col">
      <?php       
        //include 'get_dogo.php'; 
        if (isset($_GET["raza"])) {
          $geting_raza =  $_GET["raza"];  
          $r_img_url = get_img($geting_raza);
          $subrazas = get_subraza($geting_raza);
          echo "<div class='row'>
                  <div class='col'>
                    <h2>$geting_raza</h2>
                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,</p> 
                  </div>
                  <div class='col'>
                    <img src='$r_img_url' class='rounded' alt='Cinque Terre' width='250'> 
                  </div>
                </div>
          ";
         //
          if(!empty($subrazas)){
            echo "<h2>SubRazas</h2>";
            foreach ($subrazas as $key => $value) {
              $sr_img_url = (get_subraza_img($geting_raza,$value)==null) ? 'dogoimg.jpg' : get_subraza_img($geting_raza,$value);
              echo "<br/><h3>$value</h3>
              &nbsp<img src='$sr_img_url' class='rounded-circle' alt='Cinque Terre' width='150'>
                    <br/>
                    ";
            }
          }
          
        }else{
          
          $geting_raza = null;
        }
      
     
      //

      ?>
    </div>
  </div>
  
  </div>
  
  </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html> 